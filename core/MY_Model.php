<?php  if ( ! defined('BASEPATH'))  { exit('No direct script access allowed'); }

/**
 * The base model, contains setup for all other models
 * @author Nick Shontz <nick.shontz@umontana.edu>
 * 
 */
class MY_Model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
}
/**
 * The Database Model loads the db driver and lets up error logging
 */
class DB_Model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        log_message('debug', 'MY_Model Class Initialized.');
    }
    
    function get_next_id($table, $id_field) {
        $next_id = 1;
        $this->db->select_max($id_field, 'MAX');
        $row = $this->db->get($table)->row();
        if (isset($row->MAX)) {
            $next_id = $row->MAX + 1;
        }
        return $next_id;
    }

    // Log as Error each time none-existing method called.
    public function __call($name, $arguments) {
        $args = implode(',',$arguments);
        log_message('error', $this->modelname.'-> '.$name.'('.$args.') Not exists.');
        return FALSE;
    }
}