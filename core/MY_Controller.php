<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * The MY_Controller extends CI_controller is never extended directly by main controllers 
 * @author Nick Shontz <nick.shontz@umontana.edu>
 */
class MY_Controller extends CI_Controller {

    var $data;

    /**
     * Constructor for MY_Controller, instantiates $this->data loads session and
     * security/authentication information and manages the profiler
     */
    public function __construct() {
        parse_str($_SERVER['QUERY_STRING'], $_GET);
        parent::__construct();
        $this->load_packages(array(APPPATH.'third_party/umt/security/', APPPATH.'third_party/umt/common/', APPPATH.'third_party/umt/cacher/'));

        $this->load->helper(array('url'));

        $this->data = array();
        $this->load->library(array('session', 'umsecurity'));

        //enable the profiler for debugging
        if (isset($_REQUEST['debug'])) {
            switch ($_REQUEST['debug']) {
                case 'true':
                    $this->session->set_userdata('debug', true);
                    break;
                default:
                    $this->session->set_userdata('debug', false);
                    break;
            }
        }

        if (isset($this->session) && $this->session->userdata('debug') && $this->session->userdata('admin')) {
            $this->session->set_userdata('environment', ENVIRONMENT);
            $this->output->enable_profiler(TRUE);
        }

        $this->benchmark->mark('template_building_start');
        // build the headcalls seciton and the login section, showing username and messages if they are logged in
        $this->data['template'] = array("title" => "The University of Montana",
            "head_calls" => $this->load->view('template/head_calls', $this->data, true),
            "content" => "");
        $this->benchmark->mark('template_building_end');
    }
    public function load_packages($packages = "") {
        if(is_string($packages) && is_dir($packages)) {
            $this->load->add_package_path($packages);
        } else {
            foreach($packages as $package) {
                if(is_dir($package)) {
                    $this->load->add_package_path($package);
                }
            }

        }
    }

    /**
     * allows logout to be called from any level
     */
    public function logout() {
        $_SESSION = array();
        $this->session->sess_destroy();
        $this->umsecurity->logout();
    }
}

/**
 * Public controller is extended by all public controllers
 */
class Public_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }
}
