<?php

/**
 * gets the http status for a given url
 * @param type $url
 * @return int 
 */
function get_status($url) {
    $reponse = @file_get_contents($url);
    $status = 200;
    if ($reponse == false) {
        $status = 500;
    }
    return $status;
}
/**
 * Gets content from an authenticated url, specifically used in grouper
 * @param string $host
 * @param int $port
 * @param string $file
 * @param string $username
 * @param string $password
 * @return string 
 */
function get_file_contents($host, $port, $file, $username="", $password="") {

    $location = $host . ":" . $port . "/" . $file;
    $result = null;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $location);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if (!empty($password) && !empty($username)) {
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    }
    curl_setopt($ch, CURLOPT_USERAGENT, 'UMTSpider/1 (+http://www.umt.edu/search)');
    curl_setopt($ch, CURLOPT_USERAGENT, "Proventum Proxy/1.0 (Linux)");
    curl_setopt($ch, CURLOPT_TIMEOUT, 10); //times out after 10s
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

?>
